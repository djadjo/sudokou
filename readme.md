Activité 3 du module C306 MIAGE
Exercice : Développement collaboratif - Solveur de Sudoku à développer en groupe
On utilisera un gestionnaire de versions Git ou SVN ou CVS permettant à l'ensemble du groupe de travail
sur le code.
On utilisera la plateforme d'intégration continue Jenkins
On ne cherche pas à évaluer les performances, cela fera l'objet de l'exercice suivant, l'implémentation
devra donc juste répondre au problème, ce n'est pas génant si la solution n'est pas performante.
On n'utilisera pas de bibliothèque externe et on ne récupèrera pas de solution toute faite, l'intérêt est
de mettre en oeuvre les problématiques du cours.
Etapes
Constitution des groupes
Dépôt du code déjà écrit par chaque groupe (reprendre le code de l'activité 2)
Implémentation du programme pour résoudre une grille:
1. écriture des signatures des méthodes (+ doc)
2. écriture des tests
3. implémentation
Exercice : Optimisation du programme réalisé dans l'exercice 1
tchat : échanges pour définir un protocole de test des performances commun
dépôt de grilles de test
production de résultat de tests de performances sur le programme existant
refactorisation du code pour améliorer les performances, le groupe devra fournir la progression des
performances au fur et à mesure de la refactorisation