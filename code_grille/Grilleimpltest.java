package grille;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * @author wouassi
 *
 */
public class GrilleImplTest {

    /**.
     * Instanciation de la grille
     */
    private GrilleImpl grille = new GrilleImpl(9);


    /**.
     * on Teste le calcul de la dimension de la grille
     */
    @Test
    public final void testGetDimension() {
        assertEquals(9, grille.getDimension());
    }

    /**.
     * on Teste l'exception lorsque x est hors bornes
     */
    @Test
    public final void testSetValue1() {
        try {
            grille.setValue(10, 5, '4');
            fail("l'exception pour x aurait du être levée");
          } catch (IllegalArgumentException e) {
          }
    }

    /**.
     * Teste l'exception lorsque y est hors bornes
     */
    @Test
    public final void testSetValue2() {
        try {
            grille.setValue(5, 10, '4');
            fail("l'exception pour le paramètre erroné aurait du être levée");
          } catch (IllegalArgumentException e) {
          }
    }

    /**.
     * on Teste l'exception lorsque value est hors bornes
     */
    @Test
    public final void testSetValue3() {
        try {
            grille.setValue(6, 5, 'h');
            fail("l'exception h aurait du être levée");
          } catch (IllegalArgumentException e) {
          }
    }

    /**.
    * on Teste la récupération d'une valeur dans la grille
    */
    @Test
    public final void testGetValue() {
            assertEquals('@', grille.getValue(6, 5));
    }

    /**.
    *on Teste l'exception lorsque x est hors bornes
    */
    @Test
    public final void testGetValue1() {
        try {
            grille.getValue(22, 4);
            fail("l'exception pour x aurait du être levée");
          } catch (IllegalArgumentException e) {
          }
    }

    /**.
    * Teste l'exception lorsque y est hors bornes
    */
    @Test
    public final void testGetValue2() {
        try {
            grille.getValue(6, 11);
            fail("l'exception pour y aurait du être levée");
          } catch (IllegalArgumentException e) {
        
          }
    }

    /**.
    * Teste si la grille est complète
    */
    @Test
    public final void testComplete() {
        assertEquals(false, grille.complete());
    }

    /**.
    * On Teste si la valeur est possible ou si elle existe déja dans la grille
    */
    @Test
    public final void testPossible() {
        assertEquals(true, grille.possible(2, 4, '2'));
    }
    /**.
    * Teste l'exception lorsque y est hors bornes
    */
    @Test
    public final void testPossible1() {
        try {
            grille.possible(5, 10, 'a');
            fail("l'exception pour a aurait due etre levée");
          } catch (IllegalArgumentException e) {
          }
    }
    /**.
    * on Teste l'exception lorsque x est hors bornes
    */
    @Test
    public final void testPossible2() {
        try {
            grille.possible(11, 2, 'a');
            fail("l'exception pour a aurait du être levée");
          } catch (IllegalArgumentException e) {
          }
    }
    /**.
    * Teste l'exception lorsque value est hors bornes
    */
    @Test
    public final void testPossible3() {
        try {
            grille.possible(4, 2, 'k');
            fail("l'exception de k aurait du être levée");
          } catch (IllegalArgumentException e) {
     
          }
    }
