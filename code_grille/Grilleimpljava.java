package grille;

/**
 * @author wouassi
 *
 */
public class GrilleImpl implements Grille {


    /**.
     * attribut grille: matrice de char
     */
    private char[][] grille;


    /**.
     * construteur de la classe
     * @param dim est la dimension de la grille
     */
    public GrilleImpl(final int dim) {

        grille = new char[dim][dim];

        //Initialiser chaque case de la grille avec la constance EMPTY
        for (int i = 0; i < grille.length; i++) {
            for (int j = 0; j < grille.length; j++) {
                grille[i][j] = EMPTY;

            }
        }
        //this.grille = grille1;

    }

    @Override
    public final int getDimension() {
        // TODO Auto-generated method stub
        //retourne la taille du tableau
        return grille.length;
    }

    @Override
    public final void setValue(final int x, final int y, final char value)
            throws IllegalArgumentException {
        // TODO Auto-generated method stub

        boolean existe = false;

        //On vérifie que les paramètres ne sont pas hors bornes
        if ((x < 0 || x > this.getDimension()) || (y < 0 || y > this.getDimension())) {
            throw new IllegalArgumentException();
        }
        if ((value < '1' || value > '9') && (value < 'a' || value > 'f')) {
            throw new IllegalArgumentException();
        }

        //on parcourt la grille pour savoir si la valeur existe déjà
        for (int i = 0; i < grille.length; i++) {
            if (grille[x - 1][i] == value) {
                existe = true;
            }
            if (grille[i][y - 1] == value) {
                existe = true;
            }

        }
        if (existe) {
            throw new IllegalArgumentException();
        }
        grille[x-1][y-1] = value;

    }

    @Override
    public final char getValue(final int x, final int y)
            throws IllegalArgumentException {
        // TODO Auto-generated method stub
        if ((x < 0 || x > this.getDimension()) || (y < 0 || y > this.getDimension())) {
            throw new IllegalArgumentException();
        }
        return grille[x-1][y-1];
    }

    @Override
    public final boolean complete() {
        // TODO Auto-generated method stub
        for (int y = 0; y < getDimension(); y++) {
            for (int x = 0; x < getDimension(); x++) {
                if (grille[y][x] == EMPTY) {
                return false;
                }
            }
        }
        return true;
    }

    @Override
    public final boolean possible(final int x, final int y, final char value)
            throws IllegalArgumentException {
        // TODO Auto-generated method stub
        boolean valeurPossible = false;

        //Tests sur les bornes des valeurs
        if ((x < 0 || x > this.getDimension()) || (y < 0 || y > this.getDimension())) {
            throw new IllegalArgumentException();
        }
        //on vérifie que le paramètre value correspond à
        //une valeur de POSSIBLE[]
        for (int i = 0; i < POSSIBLE.length; i++) {
            if (POSSIBLE[i] == value) {
                valeurPossible = true;
                break;
            } else if (i == POSSIBLE.length - 1 && !valeurPossible) {
                throw new IllegalArgumentException();
            } //else {
                //continue;
            //}
        }
        //System.out.println("verifCarre");
      //teste si valeur déjà choisie dans ligne/colonne
        for (int i = 0; i < grille.length; i++) {
            System.out.println("x="+x+" et i="+i+"      y="+y);
            if ((grille[x-1][i] == value) || grille[i][y-1] == value ) {
                valeurPossible = false;
                //return false;
                
            }
        
        }
        

        // Tesssssssssssst
        //System.out.println("verifCarre");
        int div = 0;
        switch (this.getDimension()) {
            case 9:
                div = 3;
                break;
            case 16:
                div = 4;
                break;
        }
        Carre carre = new Carre(div);
        if (!carre.verifCarre(grille, x, y, value)) {
            
            //return false;
            valeurPossible = false;
        }
        

        
//        for (int i = 0; i < grille.length; i++) {
//            
//        }
        return valeurPossible;
    }


}