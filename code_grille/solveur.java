package main.java;

/**
 * @author wouassi
 *
 */
public class Solveur {

    /**
     * methode qui résoud la grille de sudoku donnée en paramètre.
     * @param grille la grille à résoudre de type {@link GrilleImp}
     * @return la grille résolu de type {@link GrilleImp}
     */
    public final GrilleImpl resoudre(GrilleImpl grille) {
        while (!grille.complete()) {
            for (int x = 0; x < grille.getDimension(); x++) {
                for (int y = 0; y < grille.getDimension(); y++) {
                    if (grille.getValue(x, y) == grille.EMPTY) {
                        for (int ch = 0; ch < grille.POSSIBLE.length; ch++) {
                            if (grille.possible(x, y, grille.POSSIBLE[ch])) {
                                grille.setValue(x, y, grille.POSSIBLE[ch]);
                            }
                        }
                    }
                }
            }
        }
        return grille;
    }
}